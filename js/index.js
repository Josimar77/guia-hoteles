
$(function () {
   
  $('[data-toggle="tooltip"]').tooltip();
  $('.btn-reserva').popover({
    placement : 'top',
    trigger : 'hover'
  });
  $('.carousel').carousel({
    interval: 5000
  });

    
  $('#contacto').on('show.bs.modal', function(e) {
    console.log('El modal contacto se esta mostrando');
    $('#contactoBtn').removeClass('btn-outline-success');
    $('#contactoBtn').addClass('btn-primary');
    $('#contactoBtn').prop('disabled',true);

  });
  $('#contacto').on('shown.bs.modal', function(e) {
    console.log('El modal contacto se esta mostró');
  });
  $('#contacto').on('hide.bs.modal', function(e) {
    console.log('El modal contacto se oculta');
    $('#contactoBtn').removeClass('btn-primary');
    $('#contactoBtn').addClass('btn-outline-success');
    $('#contactoBtn').prop('disabled',false);


  });
  $('#contacto').on('hidden.bs.modal', function(e) {
    console.log('El modal contacto se ocultó');

  });

});
