module.exports = function (grunt){
	const sass = require('node-sass');

	require('time-grunt')(grunt);
	require('jit-grunt')(grunt, {
         // for custom tasks.
		useminPrepare: 'grunt-usemin',
		filerev: 'grunt-filerev'
	});

	 // Project configuration.

	 grunt.initConfig({
	 	pkg: grunt.file.readJSON('package.json'),
	 	sass: {
	 		dist: {
	 			files: [{
	 				expand: true,
	 				cwd: 'css',
	 				src: ['*.scss'],
	 				dest: 'css',
	 				ext: '.css'
	 			}]
	 		}
	 	},

	 	watch: {
	 		files: ['css/*.scss'],
	 		tasks: ['css']
	 	},

	 	browserSync: {
	 		dev: {
	 			bsFiles: {
	 				src : [ //browser files
	 				'css/*.css',
	 				'*.html',
	 				'js/*.js'
	 				]
	 			},
	 			options: {
	 				watchTask: true,
	 				server: {
	 					baseDir: "./" //directorio base de nuestro servidor
	 				}
	 			}
	 		}
	 	},
	 	imagemin: {
	 		dinamic: {
	 			files: [{
	 				expand: true,
	 				cwd: './',
	 				src: 'images/*.{png,gif,jpg,jpeg,webp}',
	 				dest: 'dist/'
	 			}]
	 		}
	 	},
	 	
	 	copy: {
	 		html: {
	 			files: [{
	 				expand: true,
	 				dot: true,
	 				cwd: './',
	 				src: ['*.html'],
	 				dest: 'dist'
	 			}]
	 		},
	 		fonts: {
	 			files: [{
	 				expand: true,
	 				dot: true,
	 				cwd: 'node_modules/open-iconic/font',
	 				src: ['fonts/*.*'],
	 				dest: 'dist'
	 			}]

	 		}
	 	},

	 	clean: {
	 		build: {
	 			src: ['dist/'],
	 		}

	 	},

	 	cssmin: {
	 		dist: {}

	 	},

	 	uglify: {
	 		dist: {}

	 	},

	 	filerev: {
	 		options: {
	 			encoding: 'utf8',
	 			algorithm: 'md5',
	 			length: 20
	 		},

	 		release: {
	 			files: [{
	 				src: [
	 				'dist/js/*.js',
	 				'dist/css/*.css',
	 				]
	 			}]
	 		}

	 	},

	 	concat: {
	 		options: {
	 			separator: ';'
	 		},
	 		dist: {}
	 	},

	 	useminPrepare: {
	 		foo: {
	 			dest: 'dist',
	 			src: ['index.html','abaut.html','precios.html','contacto.html','terminosCondiciones.html']
	 		},
	 		options: {
	 			flow: {
	 				steps: {
	 					css: ['cssmin'],
	 					js: ['uglify']
	 				},
	 				post: {
	 					css: [{
	 						name: 'cssmin',
	 						createConfig: function(context, block) {
	 							var generated = context.options.generated;
	 							generated.options = {
	 								keepSpecialComments: 0,
	 								rebase: false
	 							}
	 						}
	 					}]
	 				}
	 			}
	 		}

	 	},

	 	usemin: {
	 		html: ['dist/index.html','dist/abaut.html','dist/precios.html','dist/contacto.html','dist/terminosCondiciones.html'],
	 		options: {
	 			assetDir: ['dist', 'dist/css', 'dist/js']
	 		}
	 	}


	 });


	
	 grunt.registerTask('css', ['sass']);   
	 grunt.registerTask('default', ['browserSync','watch']);  
	 grunt.registerTask('img:compress', ['imagemin']);   
	 grunt.registerTask('build', [
	 	'clean',
	 	'copy',
	 	'imagemin',
	 	'useminPrepare',
	 	'concat',
	 	'cssmin',
	 	'uglify',
	 	'filerev',
	 	'usemin'
	 	]) 

	};
